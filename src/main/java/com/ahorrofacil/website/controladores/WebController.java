package com.ahorrofacil.website.controladores;

import java.util.List;

import com.ahorrofacil.website.modelos.Web;
import com.ahorrofacil.website.repositorios.WebRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/web")

public class WebController {

    @Autowired
    WebRepository webRepository;


    @GetMapping("/web")
    public ResponseEntity<List<Web>> findWebByUserId(@RequestParam(required = true) long id) {
        List<Web> web = webRepository.findByUserId(id);
        return new ResponseEntity<>(web, HttpStatus.OK);
    }

    @PostMapping("/web")
    public ResponseEntity<Web> registerWeb(@RequestBody Web web) {
        try {
            Web created = webRepository.save(
                    new Web(web.getTitle(),  web.getUserId(), web.isActive()));
            return new ResponseEntity<>(created, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    
}
