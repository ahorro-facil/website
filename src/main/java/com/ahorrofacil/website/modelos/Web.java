package com.ahorrofacil.website.modelos;

import javax.persistence.*;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Getter
@Setter
@NoArgsConstructor
@Table(name = "web")


public class Web {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "title")
    private String title;

    @Column(name = "user_id")
    private long userId;

    @Column(name = "active")
	private boolean active;

    public Web(String title , long userId, boolean active) {
        this.title = title;
    
        this.userId = userId;
        this.active = active;
    }
    
}
