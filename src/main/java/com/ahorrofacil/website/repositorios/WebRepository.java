package com.ahorrofacil.website.repositorios;
import java.util.List;

import com.ahorrofacil.website.modelos.Web;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;


public interface WebRepository extends JpaRepository<Web, Long> {

    @Query(
        value = "SELECT * FROM web i WHERE i.user_id = ?1",
        nativeQuery = true
    )
    List<Web> findByUserId(long userId);



    
}
