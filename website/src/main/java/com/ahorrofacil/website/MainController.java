package com.ahorrofacil.website;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/website")
public class MainController {
    @GetMapping
    public String saludo(){
        return "Hello from web";
    }
}
